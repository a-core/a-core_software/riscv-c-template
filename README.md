# RISC-V C Program Template
This repository contains an example configuration of a RV32IM C-language program executing on the A-Core processor.
The set of enabled hardware features can be controlled with the `-march` and `-mabi` flags.

Note that this repository is not usable as-is, you must use it as part of `riscv-assembly-tests` repository which provides the default Makefile and linker script. Naturally, you may also write your own Makefile and/or linker script specific for this software program. You can also include the default Makefile and override some symbols, such as `MARCH` and `MABI`.

# Repository Structure
* README.md -- you are reading it right now
* configure -- script for generating build configuration files
* a-core.ld -- [linker script](https://sourceware.org/binutils/docs/ld/Scripts.html) for the A-Core target. Includes default linker script.
* main.c -- example C language program
* crt0.s -- sets up C execution environment and jumps to main
* Makefile -- build system configuration for GNU make. Includes default makefile and overrides `MARCH` and `MABI` symbols..

# Linking
The linking process is controlled by the linker script `a-core.ld` which contains configurations for binaries
targeting A-Core. For reference, the default RISC-V linker script for 32-bit binaries can be viewed with
`riscv64-unknown-elf-ld -march=elf32lriscv --verbose`.

# Compilation Instructions
The program can be compiled with `make`.
