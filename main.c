#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "a-core-utils.h"
#include "a-core.h"
#include "acore-gpio.h"

// statically initialize some data in .data section
int result = 3;

// compute factorial of an integer recursively
int factorial(int n) {
    if (n == 0)
        return 1;
    else
        return n*factorial(n-1);
}

void main() {

    // Count program time
    clock_t start_t, end_t;
    start_t = clock();
    uint64_t start_instret, end_instret;
    start_instret = get_instret();

    // Init UART
    volatile uint32_t* uart_base_addr = (volatile uint32_t*) A_CORE_AXI4LUART;
    init_uart(uart_base_addr, BAUDRATE);
    
    // printf and byte transmit
    printf("Welcome to A-Core!\n");
    transmit_byte((volatile uint8_t*) uart_base_addr, 'B');
    transmit_byte((volatile uint8_t*) uart_base_addr, '\n');

    // gpios
    volatile uint32_t* gpo_addr = (volatile uint32_t*) (A_CORE_AXI4LGPIO+GPIO_OUT_REG);
    volatile uint32_t* gpi_addr = (volatile uint32_t*) (A_CORE_AXI4LGPIO+GPIO_IN_REG);
    gpo_write(gpo_addr, 0b10);
    printf("Input IOs: 0x%lx\n", gpi_read(gpi_addr));
    printf("Bit 0: %ld\n", gpi_read_bit(gpi_addr, 0));
    printf("Bit 1: %ld\n", gpi_read_bit(gpi_addr, 1));
    printf("Bit 4: %ld\n", gpi_read_bit(gpi_addr, 4));

    end_instret = get_instret();
    end_t = clock();
    long int total_t = (long int)(end_t - start_t);
    long int total_instret = (long int)(end_instret - start_instret);
    printf("Program duration: %ld cycles\n", total_t);
    printf("Instructions retired: %ld\n", total_instret);

    disable_f(); // Test disabling F extension
    // run some test functions
    int a = 12;
    result = factorial(a);
    enable_f();

    if (result == 479001600) {
        test_pass();
    }
    else {
        test_fail();
    }
    test_end();
}
